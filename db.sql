CREATE TABLE `uld_right` (
  `id` int(11) NOT NULL,
  `right` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `uld_right` (`id`, `right`) VALUES
(1, 'Adresář'),
(2, 'Vyhledávač');

ALTER TABLE `uld_right`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `uld_right`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

------------------------------------------------------------------------------
CREATE TABLE `uld_user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `uld_user` (`id`, `name`) VALUES
(1, 'Adam'),
(2, 'Bob'),
(3, 'Cyril'),
(4, 'Derek');

ALTER TABLE `uld_user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `uld_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--------------------------------------------------------------------------------
  CREATE TABLE `uld_village` (
  `id` int(11) NOT NULL,
  `village` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `uld_village` (`id`, `village`) VALUES
(1, 'Praha'),
(2, 'Brno');

ALTER TABLE `uld_village`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `uld_village`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-------------------------------------------------------------------------------  
CREATE TABLE `uld_user_admin` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `right_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `uld_user_admin` (`id`, `user_id`, `right_id`, `village_id`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 2, 2, 1),
(4, 2, 1, 2),
(5, 3, 1, 1),
(6, 3, 1, 2),
(7, 3, 2, 2);

ALTER TABLE `uld_user_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_626A2787A76ED395` (`user_id`),
  ADD KEY `IDX_626A278754976835` (`right_id`),
  ADD KEY `IDX_626A27875E0D5582` (`village_id`);

ALTER TABLE `uld_user_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `uld_user_admin`
  ADD CONSTRAINT `FK_626A278754976835` FOREIGN KEY (`right_id`) REFERENCES `uld_right` (`id`),
  ADD CONSTRAINT `FK_626A27875E0D5582` FOREIGN KEY (`village_id`) REFERENCES `uld_village` (`id`),
  ADD CONSTRAINT `FK_626A2787A76ED395` FOREIGN KEY (`user_id`) REFERENCES `uld_user` (`id`);