<?php

namespace App\Models\Facade;

use App\Models\Entity\Right;
use App\Models\Entity\User;
use App\Models\Entity\UserAdmin;
use App\Models\Entity\Village;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Models\Repository\RightsRepository;

/**
 * Class RightsFacade
 * @package App\Models\Facade
 */
class RightsFacade
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Doctrine\ORM\RightsRepository
     */
    private $rightEntity;

    /**
     * @var \Doctrine\ORM\RightsRepository
     */
    private $userEntity;

    /**
     * @var \Doctrine\ORM\RightsRepository
     */
    private $userAdminEntity;

    /**
     * @var \Doctrine\ORM\RightsRepository
     */
    private $villageEntity;


    /**
     * RightsFacade constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->rightEntity = $this->entityManager->getRepository(Right::class);
        $this->userEntity = $this->entityManager->getRepository(User::class);
        $this->userAdminEntity = $this->entityManager->getRepository(UserAdmin::class);
        $this->villageEntity = $this->entityManager->getRepository(Village::class);
    }


    /**
     * Find user with ID.
     * @param int $id
     * @return object|null
     */
    public function findUser(int $id)
    {
        return $this->userEntity->findOneBy(['id' => $id]);
    }


    /**
     * Find village with ID.
     * @param int $id
     * @return object|null
     */
    public function findVillage(int $id)
    {
        return $this->villageEntity->findOneBy(['id' => $id]);
    }

    /**
     * Find right with ID.
     * @param int $id
     * @return object|null
     */
    public function findRight(int $id)
    {
        return $this->rightEntity->findOneBy(['id' => $id]);
    }


    /**
     * Find right by name.
     * @param string $right
     * @return object|null
     */
    public function findRightByName(string $right)
    {
        return $this->rightEntity->findOneBy(['right' => $right]);
    }


    /**
     * Get all villages.
     * @return ArrayCollection
     */
    public function getAllVillages()
    {
        $villages = $this->villageEntity->findAll();
        return new ArrayCollection($villages);
    }


    /**
     * Get all rights.
     * @return ArrayCollection
     */
    public function getAllRights()
    {
        $rights = $this->rightEntity->findAll();
        return new ArrayCollection($rights);
    }


    /**
     * Get all users.
     * @return ArrayCollection
     */
    public function getAllUsers()
    {
        $users = $this->userEntity->findAll();
        return new ArrayCollection($users);
    }


    /**
     * Find all user rights.
     * @param int $userId
     * @return ArrayCollection
     */
    public function getAllUserRights(int $userId)
    {
        $userRights = $this->userAdminEntity->findBy(['userId' => $userId]);
        return new ArrayCollection($userRights);
    }


    /**
     * Add all rights to user.
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addUserAllRights(User $user)
    {
        if ($user) {
            #get all rights
            $rights = $this->getAllRights();

            ###add all rights to new user
            foreach ($rights as $right) {
                $this->addUserRightForAllVillages($user, $right);
            }
        }
    }

    /**
     * Add chosen right for all villages to user.
     * @param User $user
     * @param Right $right
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addUserRightForAllVillages(User $user, Right $right)
    {
        #get all villages
        $villages = $this->getAllVillages();

        foreach ($villages as $village) {
            $this->addUserVillageRight($user, $right, $village);
        }
    }

    /**
     * Add chosen right for village to user.
     * @param User $user
     * @param Right $right
     * @param Village $village
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addUserVillageRight(User $user, Right $right, Village $village)
    {
        $newUserAdmin = new UserAdmin();
        $newUserAdmin->setUserId($user);
        $newUserAdmin->setRightId($right);
        $newUserAdmin->setVillageId($village);

        $this->entityManager->persist($newUserAdmin);
        $this->entityManager->flush();
    }


    /**
     * Add new user with all rights.
     * @param string $name
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewUser(string $name)
    {
        if ($name) {
            ###add user
            $user = new User();
            $user->setName($name);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            ###add all rights to new user
            $this->addUserAllRights($user);
        }
    }


    /**
     * Add new village and rights for this village users with all rights.
     * @param string $village
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewVillage(string $village)
    {
        if ($village) {
            #get number of villages
            $numberOfVillages = $this->villageEntity->createQueryBuilder('v')
                ->select('count(v.id)')
                ->getQuery()
                ->getSingleScalarResult();


            ###add new village
            $newVillage = new Village();
            $newVillage->setVillage($village);

            $this->entityManager->persist($newVillage);
            $this->entityManager->flush();


            #get all users and rights
            $users = $this->getAllUsers();
            $rights = $this->getAllRights();
            foreach ($users as $user) {
                foreach ($rights as $right) {
                    #get number of user admin villages for chosen right
                    $numberOfUserAdminVillages = $this->userAdminEntity->createQueryBuilder('ua')
                        ->select('count(ua.id)')
                        ->where('ua.userId = :userId AND ua.rightId = :rightId')
                        ->setParameter('userId', $user->getId())
                        ->setParameter('rightId', $right->getId())
                        ->getQuery()->getSingleScalarResult();

                    #if user has chosen right for all villages, add also right for new village
                    if ($numberOfVillages == $numberOfUserAdminVillages) {
                        $this->addUserVillageRight($user, $right, $newVillage);
                    } //if
                } //foreach
            } //foreach
        } //if
    }


    /**
     * Delete all user rights.
     * @param User $user
     * @return bool
     */
    public function deleteAllUserRights(User $user)
    {
        if ($user) {
            $userRights = $this->getAllUserRights($user->getId());
            foreach ($userRights as $userRight) {
                try{
                    $this->entityManager->remove($userRight);
                    $this->entityManager->flush();
                }
                catch (\Exception  $e) {
                    return false;
                }
            } //foreach
        } //if
    }


    /**
     * @param int $userId
     * @param $rightsArray
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserRights(int $userId, $rightsArray)
    {
        $user = $this->findUser($userId);

        if ($user) {
            ###delete all users rights
            $this->deleteAllUserRights($user);

            #if array is null or empty setup all rights to user
            if ($rightsArray == null || empty($rightsArray)) {
                #add all rights to user
                $this->addUserAllRights($user);
            }
            else {
                foreach ($rightsArray as $rightArrayKey => $rightArray) {
                    #find right
                    $right = $this->findRightByName($rightArrayKey);

                    if ($right) {
                        #if array with rights is empty or all rights are false setup chosen right for all villages to user
                        if ($rightArray == null || empty($rightArray) || !(in_array(true, $rightArray))) {
                            #add right to all villages
                            $this->addUserRightForAllVillages($user, $right);
                        } else {
                            #setup rights as was checked
                            foreach ($rightArray as $cityKey => $value) {
                                if ($value && $village = $this->findVillage($cityKey)) {
                                    $this->addUserVillageRight($user, $right, $village);
                                }
                            }
                        }
                    } //if
                } //foreach
            } //else
        } //if
    }


    /**
     * Return villages on which user has chosen right.
     * @param int $userId
     * @param int $rightId
     * @return array
     */
    public function getUserVillagesOfRight(int $userId, int $rightId)
    {
        #find user and right
        $user = $this->findUser($userId);
        $right = $this->findRight($rightId);

        if ($user && $right) {
            return $this->villageEntity->createQueryBuilder('v')
                ->select('v.village')
                ->join(UserAdmin::class, 'ua', 'with', 'v.id = ua.villageId')
                ->where('ua.userId = :userId AND ua.rightId = :rightId')
                ->setParameter('userId', $user->getId())
                ->setParameter('rightId', $right->getId())
                ->getQuery()->getArrayResult();
        }
    }

}