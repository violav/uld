<?php

namespace App\Models\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \Kdyby\Doctrine\Entities\Attributes\Identifier;
use App\Models\Repository\RightsRepository;


/**
 * @ORM\Entity(repositoryClass="\App\Models\Repository\RightsRepository")
 * @ORM\Table(name="`uld_user`")
 */
class User
{
    use Identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;


    /* ********************************************************************************************** */
    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }


    /* ********************************************************************************************** */
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
