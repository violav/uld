<?php

namespace App\Models\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \Kdyby\Doctrine\Entities\Attributes\Identifier;
use App\Models\Repository\RightsRepository;


/**
 * @ORM\Entity(repositoryClass="\App\Models\Repository\RightsRepository")
 * @ORM\Table(name="`uld_village`")
 */
class Village
{
    use Identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="village", type="string", length=255, nullable=false)
     */
    private $village;


    /* ********************************************************************************************** */
    /**
     * @param string $village
     */
    public function setVillage(string $village): void
    {
        $this->village = $village;
    }


    /* ********************************************************************************************** */
    /**
     * @return string
     */
    public function getVillage(): string
    {
        return $this->village;
    }
}
