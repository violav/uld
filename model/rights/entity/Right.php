<?php

namespace App\Models\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \Kdyby\Doctrine\Entities\Attributes\Identifier;
use App\Models\Repository\RightsRepository;


/**
 * @ORM\Entity(repositoryClass="\App\Models\Repository\RightsRepository")
 * @ORM\Table(name="`uld_right`")
 */
class Right
{
    use Identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="right", type="string", length=255, nullable=false)
     */
    private $right;


    /* ********************************************************************************************** */
    /**
     * @param string $right
     */
    public function setRight(string $right): void
    {
        $this->right = $right;
    }


    /* ********************************************************************************************** */
    /**
     * @return string
     */
    public function getRight(): string
    {
        return $this->right;
    }
}
