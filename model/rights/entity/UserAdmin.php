<?php

namespace App\Models\Entity;

use App\Models\Entity\User;
use App\Models\Entity\Right;
use App\Models\Entity\Village;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \Kdyby\Doctrine\Entities\Attributes\Identifier;
use App\Models\Repository\RightsRepository;


/**
 * @ORM\Entity(repositoryClass="\App\Models\Repository\RightsRepository")
 * @ORM\Table(name="`uld_user_admin`")
 */
class UserAdmin
{
    use Identifier;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $userId;

    /**
     * @var Right
     *
     * @ORM\ManyToOne(targetEntity="Right")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="right_id", referencedColumnName="id")
     * })
     */
    private $rightId;

    /**
     * @var Village
     *
     * @ORM\ManyToOne(targetEntity="Village")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="village_id", referencedColumnName="id")
     * })
     */
    private $villageId;


    /* ********************************************************************************************** */
    /**
     * @param \App\Models\Entity\Right $rightId
     */
    public function setRightId(\App\Models\Entity\Right $rightId): void
    {
        $this->rightId = $rightId;
    }

    /**
     * @param \App\Models\Entity\User $userId
     */
    public function setUserId(\App\Models\Entity\User $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @param \App\Models\Entity\Village $villageId
     */
    public function setVillageId(\App\Models\Entity\Village $villageId): void
    {
        $this->villageId = $villageId;
    }


    /* ********************************************************************************************** */
    /**
     * @return \App\Models\Entity\Right
     */
    public function getRightId(): \App\Models\Entity\Right
    {
        return $this->rightId;
    }

    /**
     * @return \App\Models\Entity\User
     */
    public function getUserId(): \App\Models\Entity\User
    {
        return $this->userId;
    }

    /**
     * @return \App\Models\Entity\Village
     */
    public function getVillageId(): \App\Models\Entity\Village
    {
        return $this->villageId;
    }
}